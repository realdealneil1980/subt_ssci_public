#!/usr/bin/env bash

. /opt/ros/melodic/setup.bash
. ~/subt_ws/install/setup.sh
. ~/workspaces/catkin_build_ws/devel/setup.bash

exec roslaunch $@
