#!/usr/bin/env bash

. /opt/ros/melodic/setup.bash
. /home/developer/subt_ws/install/setup.bash

# for local test - the sleep is required so the roslaunches aren't simultaneous
#exec ign launch -v 4 tunnel_circuit_practice.ign worldName:=tunnel_circuit_practice_02 robotName1:=XX1 robotConfig1:=X1_SENSOR_CONFIG_1 robotName2:=XX2 robotConfig2:=X2_SENSOR_CONFIG_1 robotName3:=XX3 robotConfig3:=X3_SENSOR_CONFIG_1 robotName4:=XX4 robotConfig4:=X4_SENSOR_CONFIG_1 &
#sleep 20

exec roslaunch cmd_vel_pubs.launch
