#!/usr/bin/env bash

. /opt/ros/melodic/setup.bash
. ~/subt_ws/install/setup.sh
. ~/workspaces/catkin_build_ws/devel/setup.bash

#exec ign launch -v 4 virtual_stix.ign robotName1:=alpha robotConfig1:=X1_SENSOR_CONFIG_4 &
#sleep 20

exec roslaunch helloworld_subt_launch robot.launch name:=alpha other_robot:=bravo perception:=true comms:=false control:=true fake_report:=false

