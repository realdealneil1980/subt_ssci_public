cmake_minimum_required(VERSION 2.8.3)
project(comms_example)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

set(PKG_DEPENDS
#  message_generation
  roscpp
  sensor_msgs
  subt_msgs
  subt_ign
  subt_communication_broker
  vision_msgs
  )

find_package(catkin REQUIRED ${PKG_DEPENDS})

find_package(ignition-common3 REQUIRED)
find_package(ignition-msgs4 REQUIRED)
find_package(ignition-transport7 REQUIRED)
find_package(sdformat8 REQUIRED)
find_package(ignition-plugin1 REQUIRED COMPONENTS loader register)
find_package(ignition-launch1 REQUIRED)

################################################
## Declare ROS messages, services and actions ##
################################################

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

###################################
## catkin specific configuration ##
###################################
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES comms_example
  CATKIN_DEPENDS ${PKG_DEPENDS}
#  DEPENDS system_lib
#  CFG_EXTRAS
#    ${PROJECT_NAME}-extras.cmake
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  #include
  ${CATKIN_DEVEL_PREFIX}/include
  ${catkin_INCLUDE_DIRS}
  ${ignition-transport7_INCLUDE_DIRS}
  ${ignition-msgs4_INCLUDE_DIRS}
  ${ignition-common3_INCLUDE_DIRS}
#  ${GAZEBO_INCLUDE_DIRS}
  )

#link_directories(
#  ${GAZEBO_LIBRARY_DIRS}
#)

set(project_libs
  ${catkin_LIBRARIES}
  ${ignition-transport7_LIBRARIES}
  ${ignition-msgs4_LIBRARIES}
  ${ignition-common3_LIBRARIES}
  )
  
add_executable(object_detection_node src/object_detection_node.cc src/artifact.pb.cc)
target_link_libraries(object_detection_node
  ignition-common3::ignition-common3
  ignition-plugin1::loader
  ignition-transport7::ignition-transport7
  ignition-launch1::ignition-launch1
  sdformat8::sdformat8
  ${catkin_LIBRARIES}
  #${protobuf_lib_name}
) # ${SubtProtobuf})
add_dependencies(object_detection_node ${catkin_EXPORTED_TARGETS}) # ${SubtProtobuf})

add_executable(velocity_accepter_node src/velocity_accepter_node.cc)
target_link_libraries(velocity_accepter_node
  ignition-common3::ignition-common3
  ignition-plugin1::loader
  ignition-transport7::ignition-transport7
  ignition-launch1::ignition-launch1
  sdformat8::sdformat8
  ${catkin_LIBRARIES}
) # ${SubtProtobuf})
add_dependencies(velocity_accepter_node ${catkin_EXPORTED_TARGETS}) # ${SubtProtobuf})


#############
## Install ##
#############
## Mark executables and/or libraries for installation
install(TARGETS object_detection_node
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
  )

#install(FILES cmake/${PROJECT_NAME}-extras.cmake
#  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/cmake)

#############
## Testing ##
#############
