//#include <subt_gazebo/CommsClient.hh>
//#include <subt_ign/CommonTypes.hh>
#include <subt_communication_broker/subt_communication_client.h>
//#include <subt_gazebo/CommonTypes.hh>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>


class VelocityComms
{
public:
  VelocityComms(const std::string &_name,
                 const std::string &_address,
		 const std::string &_dstname,
		 const std::string &_dstaddress);

  void VelocityCmdCallback(const geometry_msgs::Twist::ConstPtr &_vel_cmd);
  void CommandReceivedCallback(const std::string &_srcAddress,
				 const std::string &_dstAddress,
				 const uint32_t _dstPort,
				 const std::string &_data);

private:

  //  void testJSON();
  
  ros::NodeHandle n;
  ros::Subscriber velocityCmdSub;
  ros::Publisher velocityCmdPub;

  /// \brief Communication client.
  std::unique_ptr<subt::CommsClient> client;
  
  std::string client_name;
  std::string client_address;
  std::string destination_name;
  std::string destination_address;

};

VelocityComms::VelocityComms(const std::string &_name,
			       const std::string &_address,
			       const std::string &_dstname,
			       const std::string &_dstaddress)
{
 
  client_name = _name;
  client_address = _address;
  destination_name = _dstname;
  destination_address = _dstaddress;

  std::stringstream this_vel_topic;
  this_vel_topic << "/" << client_name << "/cmd_vel";

  // initializes the comms client
  this->client.reset(new subt::CommsClient(client_address));

  // subscribes to the ROS topic to send velocity commands
  this->velocityCmdSub
    = this->n.subscribe<geometry_msgs::Twist>(
        "/other/cmd_vel", 1, &VelocityComms::VelocityCmdCallback, this);


  // advertise a velocity command received on this ROS topic (to publish when receiving)
  this->velocityCmdPub
    = this->n.advertise<geometry_msgs::Twist>(
        this_vel_topic.str(), 1);

  // bind a callback function to receiving data over the comms client
  this->client->Bind(&VelocityComms::CommandReceivedCallback, this);
  
}

void VelocityComms::VelocityCmdCallback(const geometry_msgs::Twist::ConstPtr &_vel_cmd)
{
  ROS_DEBUG("velocity_accepter_node::VelocityCmdCallback");
  
  // serialize the detection array
  size_t serial_size = ros::serialization::serializationLength(*_vel_cmd);
  boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
  ros::serialization::OStream stream(buffer.get(), serial_size);
  ros::serialization::serialize(stream, *_vel_cmd);
  std::string str_msg;
  str_msg.reserve(serial_size);
  for (size_t i = 0; i < serial_size; ++i)
  {
    str_msg.push_back(buffer[i]);
  }

  // push the data through the comms client
  bool succ = this->client->SendTo(str_msg,destination_address);
  if (succ) 
    ROS_DEBUG("Successfully sent end message to %s", destination_address.c_str());
  else
    ROS_DEBUG("Failed to sent message to %s", destination_address.c_str());
}

void VelocityComms::CommandReceivedCallback(const std::string &_srcAddress,
			     const std::string &_dstAddress,
			     const uint32_t _dstPort,
			     const std::string &_data) {
  
  ROS_INFO("velocity_accepter_node::CommandReceivedCallback");
  
  // deserialize the message into the detection array
  geometry_msgs::Twist velocity_cmd;
  size_t serial_size = _data.size();
  boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
  for (size_t i = 0; i < serial_size; ++i)
    {
      buffer[i] = _data[i];
    }
  ros::serialization::IStream stream(buffer.get(), serial_size);
  ros::serialization::Serializer<geometry_msgs::Twist>::read(stream, velocity_cmd);

  // publish the data over the ROS topic
  velocityCmdPub.publish(velocity_cmd);
}

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    ROS_ERROR_STREAM("usage: client_name client_address destination_name destination_address");
    return -1;
  }

  ros::init(argc, argv, argv[1]);
  
  VelocityComms detection_comms(argv[1], argv[2], argv[3], argv[4]);
    
  ros::spin();
}
