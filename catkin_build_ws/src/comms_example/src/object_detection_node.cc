//#include <subt_gazebo/CommsClient.hh>
#include <ignition/common/Console.hh>
//#include <subt_ign/CommonTypes.hh>
#include <subt_communication_broker/subt_communication_client.h>
#include <subt_ign/protobuf/artifact.pb.h>
//#include <commandpost_relay/ArtifactReport.h>
//#include <subt_gazebo/CommonTypes.hh>
#include <ros/ros.h>
#include <vision_msgs/Detection2DArray.h>
//#include <jsoncpp/json/json.h>


class DetectionComms
{
public:
  DetectionComms(const std::string &_name,
                 const std::string &_address,
		 const std::string &_dstname,
		 const std::string &_dstaddress);

  void ObjectDetectionCallback(const vision_msgs::Detection2DArray::ConstPtr &_detection);
  void DetectionReceivedCallback(const std::string &_srcAddress,
				 const std::string &_dstAddress,
				 const uint32_t _dstPort,
				 const std::string &_data);

private:

  //  void testJSON();
  
  ros::NodeHandle n;
  ros::Subscriber objectDetectionSub;
  ros::Publisher objectDetectionPub;

  /// \brief Communication client.
  std::unique_ptr<subt::CommsClient> client;
  
  std::string client_name;
  std::string client_address;
  std::string destination_name;
  std::string destination_address;

};

DetectionComms::DetectionComms(const std::string &_name,
			       const std::string &_address,
			       const std::string &_dstname,
			       const std::string &_dstaddress)
{
 
  client_name = _name;
  client_address = _address;
  destination_name = _dstname;
  destination_address = _dstaddress;

  // initializes the comms client
  this->client.reset(new subt::CommsClient(client_address));

  // subscribes to the object detection ROS topic
  this->objectDetectionSub
    = this->n.subscribe<vision_msgs::Detection2DArray>(
        "objects", 1, &DetectionComms::ObjectDetectionCallback, this);


  // advertise an object detection ROS topic (to publish when receiving)
  this->objectDetectionPub
    = this->n.advertise<vision_msgs::Detection2DArray>(
        "received_objects", 1);

  // bind a callback function to receiving data over the comms client
  this->client->Bind(&DetectionComms::DetectionReceivedCallback, this);
  
}

void DetectionComms::ObjectDetectionCallback(const vision_msgs::Detection2DArray::ConstPtr &_detection)
{
  ROS_DEBUG("object_detection_node::ObjectDetectionCallback");
  
  // serialize the detection array
  size_t serial_size = ros::serialization::serializationLength(*_detection);
  boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
  ros::serialization::OStream stream(buffer.get(), serial_size);
  ros::serialization::serialize(stream, *_detection);
  std::string str_msg;
  str_msg.reserve(serial_size);
  for (size_t i = 0; i < serial_size; ++i)
  {
    str_msg.push_back(buffer[i]);
  }

  // push the data through the comms client
  bool succ = this->client->SendTo(str_msg,destination_address);
  if (succ) 
    ROS_DEBUG("Successfully sent end message to %s", destination_address.c_str());
  else
    ROS_DEBUG("Failed to sent message to %s", destination_address.c_str());
  

  ROS_DEBUG("object_detection_node::ObjectDetectionCallback, number of detections:%d", (int)_detection->detections.size());
  
  if (_detection->detections.size() > 0)
    {

    // We only need the position.
    ignition::msgs::Pose pose;
    pose.mutable_position()->set_x(_detection->detections[0].results[0].pose.pose.position.x);
    pose.mutable_position()->set_y(_detection->detections[0].results[0].pose.pose.position.y);
    pose.mutable_position()->set_z(_detection->detections[0].results[0].pose.pose.position.z);
    
    // Fill the type and pose.
    subt::msgs::Artifact artifact;
    //artifact.set_type(static_cast<uint32_t>(subt::ArtifactType::TYPE_BACKPACK));
    artifact.set_type(static_cast<uint32_t>(_detection->detections[0].results[0].id));
    artifact.mutable_pose()->CopyFrom(pose);

    // From subt_gazebo/include/subt_gazebo/CommonTypes.hh
    //TYPE_BACKPACK       = 0,
    //TYPE_DUCT           = 1,
    //TYPE_DRILL          = 2,
    //TYPE_ELECTRICAL_BOX = 3,
    //TYPE_EXTINGUISHER   = 4,
    //TYPE_PHONE          = 4,
    //TYPE_RADIO          = 5,
    //TYPE_RESCUE_RANDY   = 6,
    //TYPE_TOOLBOX        = 7,
    //TYPE_VALVE          = 8
    
    // Serialize the artifact.
    std::string serializedData;
    if (!artifact.SerializeToString(&serializedData)) {
      std::cerr << "ReportArtifact(): Error serializing message\n"
		<< artifact.DebugString() << std::endl;
    }

    // Report it.
    ROS_INFO("Submitting artifact of type (%d) to base station",
	     _detection->detections[0].results[0].id);
    bool report = this->client->SendTo(serializedData, "base_station");//subt::kBaseStationName);
    if(report) {
      ROS_DEBUG("SendTo reported success. Check topic /subt/score for current score.");      
    } else {
      ROS_DEBUG("SendTo reported fail.");
    }
  }
  
  // transforms detection into a JSON message
  /*
  if (_detection->detections.size() > 0)
    {
  
      Json::Value json_val;
      json_val["x"] = _detection->detections[0].results[0].pose.pose.position.x;
      json_val["y"] = _detection->detections[0].results[0].pose.pose.position.y;
      json_val["z"] = _detection->detections[0].results[0].pose.pose.position.z;
      json_val["type"] = (int)_detection->detections[0].results[0].id;
  
      Json::FastWriter fastWriter;

      ROS_INFO("JSON generated:%s",fastWriter.write(json_val).c_str());

      this->client->SendTo(fastWriter.write(json_val),destination_address);
    }
  */
}

void DetectionComms::DetectionReceivedCallback(const std::string &_srcAddress,
			     const std::string &_dstAddress,
			     const uint32_t _dstPort,
			     const std::string &_data){
  
  ROS_INFO("object_detection_node::DetectionReceivedCallback");

  
  // deserialize the message into the detection array
  vision_msgs::Detection2DArray detectionArray;
  size_t serial_size = _data.size();
  boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
  for (size_t i = 0; i < serial_size; ++i)
    {
      buffer[i] = _data[i];
    }
  ros::serialization::IStream stream(buffer.get(), serial_size);
  ros::serialization::Serializer<vision_msgs::Detection2DArray>::read(stream, detectionArray);

  // publish the data over the ROS topic
  objectDetectionPub.publish(detectionArray);

  /*
  Json::Reader reader;
  Json::Value obj;
  reader.parse(_data, obj);

  Json::FastWriter fastWriter;
  ROS_INFO("JSON received:%s",fastWriter.write(obj).c_str());
  
  vision_msgs::Detection2DArray detectionArray;
  vision_msgs::Detection2D d2d;
  vision_msgs::ObjectHypothesisWithPose obwp;

  obwp.pose.pose.position.x = obj["x"].asUInt();
  obwp.pose.pose.position.y = obj["y"].asUInt();
  obwp.pose.pose.position.z = obj["z"].asUInt();

  d2d.results.push_back(obwp);
  detectionArray.detections.push_back(d2d);
  
  objectDetectionPub.publish(detectionArray);
  */    
}

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    ROS_ERROR_STREAM("usage: client_name client_address destination_name destination_address");
    return -1;
  }

  ros::init(argc, argv, argv[1]);
  
  DetectionComms detection_comms(argv[1], argv[2], argv[3], argv[4]);
    
  ros::spin();
}
