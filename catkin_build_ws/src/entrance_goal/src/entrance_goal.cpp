#include <ros/ros.h>

#include <tf/transform_listener.h>

// SubT Comm
#include <subt_msgs/PoseFromArtifact.h>

#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// ======================================================================
// Compute turn rate
// ======================================================================

int main(int argc, char **argv)
{
  ros::init(argc, argv, "entrance_goal");
  ros::NodeHandle nh, pnh("~");
  ros::ServiceClient client = nh.serviceClient<subt_msgs::PoseFromArtifact>("/subt/pose_from_artifact_origin");
  
  // read robot name from launch file
  std::string base_link, robot_name;
  pnh.param("base_link", base_link, std::string("X1/base_link"));
  pnh.param("name", robot_name, std::string("X1"));
  int start_delay;
  pnh.param("start_delay", start_delay, 0);
  ros::Duration(start_delay).sleep();
  subt_msgs::PoseFromArtifact srv;
  std_msgs::String robot_name_msg;
  robot_name_msg.data = robot_name;
  srv.request.robot_name = robot_name_msg;
  
  geometry_msgs::PoseStamped pose_to_artifact_origin;
  // Grab pose from artifact origin
  if (client.call(srv))
  {
    if(srv.response.success == true)
      pose_to_artifact_origin = srv.response.pose;
  }
  else
  {
    ROS_ERROR("Failed to successfully call /subt/pose_from_artifact_origin");
    return 1;
  }
  
  // Tunnel entrance
  geometry_msgs::Pose entrance;
  entrance.position.x = 0.0;
  entrance.position.y = 0.0;
  geometry_msgs::Pose artifact_origin;
  artifact_origin.position.x = 0.0;
  artifact_origin.position.y = 0.0;
  
  //ROS_INFO("base_link: %s", base_link.c_str());
  tf::TransformListener tf;
  MoveBaseClient ac("move_base", true);
   //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }
  move_base_msgs::MoveBaseGoal mb_goal;
  // Pull move base goal from service callback to artifact origin, to get location if robot at different start
  //
  mb_goal.target_pose.header.frame_id = "map";
  mb_goal.target_pose.header.stamp = ros::Time::now();
  mb_goal.target_pose.pose.position.x = entrance.position.x - artifact_origin.position.x - pose_to_artifact_origin.pose.position.x; //6.511;
  mb_goal.target_pose.pose.position.y = entrance.position.y - artifact_origin.position.y - pose_to_artifact_origin.pose.position.y; //-5.361;
  mb_goal.target_pose.pose.orientation.w = 1.0;
  ROS_INFO("Sending goal");
  ac.sendGoal(mb_goal);
  ac.waitForResult(ros::Duration(30.0));
  ROS_INFO("Wall follow: Arrived at initial coordinates (tunnel entrance). Commencing wall follow behavior.");
  return 0;
}

