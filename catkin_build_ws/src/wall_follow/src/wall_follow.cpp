#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/MarkerArray.h>

#include <tf/transform_listener.h>
#include <dynamic_reconfigure/server.h>
#include <wall_follow/WallFollowerConfig.h>

// SubT Comm
#include <subt_msgs/PoseFromArtifact.h>

#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm/lower_bound.hpp>
#include <boost/range/algorithm/upper_bound.hpp>
#include <boost/range/algorithm/min_element.hpp>
#include <boost/range/iterator_range.hpp>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// ======================================================================
// Compute turn rate
// ======================================================================

int main(int argc, char **argv)
{
  ros::init(argc, argv, "wall_follow");
  ros::NodeHandle nh, pnh("~");
  ros::ServiceClient client = nh.serviceClient<subt_msgs::PoseFromArtifact>("/subt/pose_from_artifact_origin");
  
  // read robot name from launch file
  std::string base_link, robot_name;
  pnh.param("base_link", base_link, std::string("X1/base_link"));
  pnh.param("name", robot_name, std::string("X1"));
  int start_delay;
  pnh.param("start_delay", start_delay, 0);
  ros::Duration(start_delay).sleep();
  subt_msgs::PoseFromArtifact srv;
  std_msgs::String robot_name_msg;
  robot_name_msg.data = robot_name;
  srv.request.robot_name = robot_name_msg;
  
  geometry_msgs::PoseStamped pose_to_artifact_origin;
  // Grab pose from artifact origin
  if (client.call(srv))
  {
    if(srv.response.success == true)
      pose_to_artifact_origin = srv.response.pose;
  }
  else
  {
    ROS_ERROR("Failed to successfully call /subt/pose_from_artifact_origin");
    return 1;
  }
  
  // Tunnel entrance
  geometry_msgs::Pose entrance;
  entrance.position.x = 0.0;
  entrance.position.y = 0.0;
  geometry_msgs::Pose artifact_origin;
  artifact_origin.position.x = 0.0;
  artifact_origin.position.y = 0.0;
  
  //ROS_INFO("base_link: %s", base_link.c_str());
  tf::TransformListener tf;
  MoveBaseClient ac("move_base", true);
   //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }
  move_base_msgs::MoveBaseGoal mb_goal;
  // Pull move base goal from service callback to artifact origin, to get location if robot at different start
  //
  mb_goal.target_pose.header.frame_id = "map";
  mb_goal.target_pose.header.stamp = ros::Time::now();
  mb_goal.target_pose.pose.position.x = entrance.position.x - artifact_origin.position.x - pose_to_artifact_origin.pose.position.x; //6.511;
  mb_goal.target_pose.pose.position.y = entrance.position.y - artifact_origin.position.y - pose_to_artifact_origin.pose.position.y; //-5.361;
  mb_goal.target_pose.pose.orientation.w = 1.0;
  ROS_INFO("Sending goal");
  ac.sendGoal(mb_goal);
  ac.waitForResult(ros::Duration(30.0));
  ROS_INFO("Wall follow: Arrived at initial coordinates (tunnel entrance). Commencing wall follow behavior.");
  std::stringstream ss;
  ss << "rosnode kill " << robot_name << "/move_base";
  //system(ss.str().c_str());
  system("rosnode kill move_base");

  // ------------------------------------------------------------
  // Controller parameters
  // ------------------------------------------------------------

  // Fixed forward velocity
  double v_forward;
  // Cross-track error gain
  double k_s;
  // Heading error gain
  double k_t;
  // Minimum wall-distance
  double d_wall;
  // Half-angle of the frontal cone
  float phi_front;
  // Half-angle of the side cone
  float phi_side;
  // Centerline bias
  double lambda;

  // ------------------------------------------------------------
  // Parameter server setup
  // ------------------------------------------------------------

  using ConfigServerType = dynamic_reconfigure::Server<wall_follow::WallFollowerConfig>;
  ConfigServerType config_server(pnh);
  // Read out the relevant parameters
  config_server.setCallback(
      [&](const auto& config, uint32_t level)
      {
        v_forward = config.v_forward;
        k_s = config.k_s;
        k_t = config.k_t;
        d_wall = config.d_wall;
        phi_front = static_cast<float>(config.phi_front * M_PI / 180.);
        phi_side = static_cast<float>(config.phi_side * M_PI / 180.);
        lambda = config.lambda;
      });

  // ------------------------------------------------------------
  // Topic publishers
  // ------------------------------------------------------------

  // Command channel
  auto cmd_pub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 10);
  // Visualization of controller
  auto vis_pub = nh.advertise<visualization_msgs::MarkerArray>("control_markers", 10);

  // ------------------------------------------------------------
  // Scan-processing callback
  // ------------------------------------------------------------

  auto callback = [&](const sensor_msgs::LaserScan::ConstPtr& msg)
  {
    if (!tf.canTransform(base_link, msg->header.frame_id, msg->header.stamp))
      return;

    // The (angle, range) view of the scan
    auto compute_angle = [da=msg->angle_increment, d0=msg->angle_min](const auto& e)
    {
      return std::make_pair(
          e.index() * da + d0,
          e.value()
          );
    };
    auto combined = msg->ranges
      | boost::adaptors::indexed(0)
      | boost::adaptors::transformed(std::ref(compute_angle))
      ;

    // Helper function to select a sub-range by angles
    auto subrange = [](const auto& rng, float min, float max)
    {
      // Get first iterator greater than min
      auto it0 = std::upper_bound(rng.begin(), rng.end(),
          std::make_pair(min, 0.f));
      auto it1 = std::lower_bound(it0, rng.end(),
          std::make_pair(max, 0.f));
      return boost::make_iterator_range(it0, it1);
    };

    // Compare scan elements by range
    auto compare = [](const auto& e1, const auto& e2)
    {
      return (e1.second < e2.second);
    };
    // Get the closest point in the left section
    auto right = std::min(
        std::make_pair(static_cast<float>(-M_PI_2), msg->range_max),
        *boost::range::min_element(
          subrange(combined, -phi_side, -phi_front),
          compare),
        compare);
    // Get the closest point in the front
    auto front = std::min(
        std::make_pair(0.f, msg->range_max),
        *boost::range::min_element(
          subrange(combined, -phi_front, phi_front),
          compare),
        compare);
    // Get the closest point in the right section
    auto left = std::min(
        std::make_pair(static_cast<float>(M_PI_2), msg->range_max),
        *boost::range::min_element(
          subrange(combined, phi_front, phi_side),
          compare),
        compare);

    // Get corresponding point
    auto p1 = left.second  * tf::Point(cosf(left.first),  sinf(left.first),  0.);
    auto p2 = right.second * tf::Point(cosf(right.first), sinf(right.first), 0.);
    // Convert to base frame coordinates
    tf::Stamped<tf::Point> q1, q2;
    tf.transformPoint(base_link,
        tf::Stamped<tf::Point>(p1, msg->header.stamp, msg->header.frame_id), q1);
    tf.transformPoint(base_link,
        tf::Stamped<tf::Point>(p2, msg->header.stamp, msg->header.frame_id), q2);

    // How wide is our corridor?
    auto L = (q2-q1).length();
    // Corridor "normal" direction
    auto u = (q2-q1).normalized();
    // Get the left- and right-most legal points
    tf::Point s1, s2;
    if (L <= 2.*d_wall)
      s1 = s2 = (q1 + q2) / 2;
    else
    {
      s1 = q1 + d_wall * u;
      s2 = q2 - d_wall * u;
    }
    // Determine where in the corridor we bias towards
    auto s = lambda * s2 + (1 - lambda) * s1;
    // Get the forward normal direction
    auto v = tf::Vector3(-u.y(), u.x(), 0.).normalized();

    // Compute the cross-track error
    double ds = s.y() - s.x()*v.y()/v.x();
    // Compute the heading error
    double dt = atan2(v.y(), v.x());

    // Our control law to reduce cross-track and heading error
    geometry_msgs::Twist vel;
    vel.linear.x = v_forward;
    vel.angular.z = fabs(v_forward)*(
        // cross-track error term
        k_s * (fabs(dt) > 0.01 ? sin(dt) / dt : 1.) * ds +
        // heading error term
        k_t * dt
        );
    cmd_pub.publish(vel);

    visualization_msgs::MarkerArray markers;
    // Plot the line between the left and right walls
    {
      visualization_msgs::Marker marker;
      marker.header.stamp = msg->header.stamp;
      marker.header.frame_id = base_link;
      marker.ns = "controller";
      marker.id = 0;
      marker.type = visualization_msgs::Marker::LINE_STRIP;
      marker.action = visualization_msgs::Marker::ADD;
      marker.pose.orientation.w = 1.;
      marker.scale.x = 0.05;
      marker.color.r = 0;
      marker.color.g = 1;
      marker.color.b = 0;
      marker.color.a = 1;
      marker.points.resize(2);
      tf::pointTFToMsg(q1, marker.points[0]);
      tf::pointTFToMsg(q2, marker.points[1]);
      markers.markers.push_back(marker);
    }
    // Plot the lane we are following
    {
      visualization_msgs::Marker marker;
      marker.header.stamp = msg->header.stamp;
      marker.header.frame_id = base_link;
      marker.ns = "controller";
      marker.id = 1;
      marker.type = visualization_msgs::Marker::ARROW;
      marker.action = visualization_msgs::Marker::ADD;
      marker.pose.orientation.w = 1.;
      marker.scale.x = 0.05;
      marker.scale.y = 0.1;
      marker.scale.z = 0.1;
      marker.color.r = 0;
      marker.color.g = 0;
      marker.color.b = 1;
      marker.color.a = 1;
      marker.points.resize(2);
      tf::pointTFToMsg(s - 2*v, marker.points[0]);
      tf::pointTFToMsg(s + 2*v, marker.points[1]);
      markers.markers.push_back(marker);
    }
    vis_pub.publish(markers);
  };
  auto sub = nh.subscribe<sensor_msgs::LaserScan>("scan", 10, callback);

  // ------------------------------------------------------------
  // Main loop
  // ------------------------------------------------------------

  ros::spin();
  return 0;
}

#if 0
//The following function is a "callback" function that is called back whenever a new laser scan is available.
//That is, this function will be called for every new laser scan.
//
// --------------------------------------------------------------------------------------------
//CHANGE THIS FUNCTION TO MAKE THE ROBOT EXPLORE INTELLIGENTLY.
// --------------------------------------------------------------------------------------------
//
    
void laser_callback ( const sensor_msgs::LaserScan::ConstPtr &scan_msg ) {
    
    laser_msg = *scan_msg;
    //data structure containing the command to drive the robot

    //Alternatively we could have looked at the laser scan BEFORE we made this decision.
    //Well, let's see how we might use a laser scan.
    float closest_left_side = scan_msg->range_max, 
          closest_front = scan_msg->range_max, 
          closest_right_side = scan_msg->range_max;
    float closest_left_beam_angle=-M_PI/2.0, closest_right_beam_angle=M_PI/2.0;
    for(size_t i = 0; i < scan_msg->ranges.size(); i++){
      if (isnan(scan_msg->ranges[i])) continue;
      double beam_angle = i * scan_msg->angle_increment + scan_msg->angle_min;
      if (beam_angle > -3*M_PI/4 && beam_angle < -0.25*M_PI/4) {
        //left side
        if (closest_left_side > scan_msg->ranges[i]) {
          closest_left_side = scan_msg->ranges[i];
          closest_left_beam_angle = beam_angle;
        }
            
      } else if (beam_angle > -0.25*M_PI/4 && beam_angle < 0.25*M_PI/4) {
        // front side
        if (closest_front > scan_msg->ranges[i])
          closest_front = scan_msg->ranges[i];
      } else if (beam_angle > 0.25*M_PI/4 && beam_angle < 3*M_PI/4) {
        // right side
        if (closest_right_side > scan_msg->ranges[i]) {
          closest_right_side = scan_msg->ranges[i];
          closest_right_beam_angle = beam_angle;
        }
      }
    }
    if (closest_front < 0.7) {
      // Need to go backwards and steer 
      // ROS_INFO("Avoiding obstacle in front!");
      motor_command.linear.x = -0.5;
      if (closest_left_side > closest_right_side)
        motor_command.angular.z = -0.1;
      else motor_command.angular.z = 0.1;
    }
    else {
       if( closest_left_side > 2.0 && closest_right_side > 2.0 && closest_front > 2.0) {
       //  ROS_INFO_STREAM("Smooth sailing");
          motor_command.linear.x = 2.0;
       } else {
          // Scale speed down by the closest obstacle
          double closest_side = (closest_left_side < closest_right_side)?closest_left_side:closest_right_side;
          closest_side = (closest_side < closest_front)?closest_side:closest_front;
          motor_command.linear.x = 1.0 - 0.8 * (1.0 - closest_side);
        //  ROS_INFO_STREAM("Slowing down to " << motor_command.linear.x);
       }
       //Now, try to balance the robot in the corridor, or seek a wall if it is far away from one
       if (closest_left_side > 3.0 && closest_right_side > 3.0) {
         motor_command.angular.z = 0.0;
       //  ROS_INFO_STREAM("And straight on till morning");
       }  else {
         if (closest_left_side +closest_right_side < 6.0) {
           // Estimate our angle to the hallway, the average of closest left and right angles
           float hallway_angle = -1.0 *(((closest_left_beam_angle+M_PI/2.0) + (closest_right_beam_angle-M_PI/2))/2.0);
           // Hallway "angle" should be zero when we are aligned to the hallway
           float lateral_offset = closest_right_side - closest_left_side;
           float k1 = 1.0, k2 = 1.0;
           if (fabs(hallway_angle > 0.01))
             motor_command.angular.z = k1 * lateral_offset * sin(hallway_angle)/hallway_angle * motor_command.linear.x - k2 * hallway_angle * fabs(motor_command.linear.x);
           else
             motor_command.angular.z = k1 * lateral_offset * motor_command.linear.x -k2 * hallway_angle * fabs(motor_command.linear.x);
           //Balance in middle
//           double diff_mag = fabs(closest_right_side - closest_left_side);
//           diff_mag = (diff_mag < 1.0)?diff_mag : 1.0;
//           motor_command.angular.z = (closest_right_side < closest_left_side)?-diff_mag:diff_mag;
    //       ROS_INFO_STREAM("Balancing in middle of corridor, hallway angle " << hallway_angle << " lateral offset " << lateral_offset << " command " << motor_command.angular.z);
         }
         else {
           //try to get 2 meters away from the nearest wall
           if (closest_left_side < closest_right_side) {
     //        ROS_INFO_STREAM("Trying to get 2 meters away from left wall");
             if (closest_left_side > 2.0) 
               motor_command.angular.z = -0.1;
             else 
               motor_command.angular.z = 0.1;
           } else {
      //       ROS_INFO_STREAM("Trying to get 2 meters away from right wall");
             if(closest_right_side > 2.0)
               motor_command.angular.z = 0.1;
             else 
               motor_command.angular.z = -0.1;
           }
         }
       }
    }
    motor_command_publisher.publish ( motor_command );
}
//
// --------------------------------------------------------------------------------------------
//


int main ( int argc, char **argv ) {
  // must always do this when starting a ROS node - and it should be the first thing to happen
  ros::init ( argc, argv, "wall_follow" );
  // the NodeHandle object is our access point to ROS
  ros::NodeHandle n;

  // Here we declare that we are going to publish "Twist" messages to the topic /cmd_vel_mux/navi
  motor_command_publisher = n.advertise<geometry_msgs::Twist> ( "cmd_vel", 100 );

  // Here we set the function laser_callback to receive new laser messages when they arrive
  laser_subscriber = n.subscribe ( "scan", 1000, laser_callback );
  // Here we set the function map_callback to receive new map messages when they arrive from the mapping subsystem

  ros::spin();
  return 0;
}
#endif
