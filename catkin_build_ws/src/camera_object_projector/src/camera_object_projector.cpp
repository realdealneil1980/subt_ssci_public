#include <ros/ros.h>
#include <vision_msgs/Detection2DArray.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>
#include <image_geometry/pinhole_camera_model.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/convert.h>


ros::Publisher object_image_pub;
ros::Publisher annotated_image_pub;
std::string dirname_to_save;
double image_save_blanking_interval = 1.0; // Seconds between each image to write
boost::shared_ptr<tf2_ros::Buffer> tfBuffer;
boost::shared_ptr<tf2_ros::TransformListener> tfListener;

bool GetMetadata(int classin, double& width, double& height, 
                 double& offset_x, double& offset_y, double& offset_z) {
  offset_x = 0; offset_y = 0; offset_z = 0;
  if (classin == 0) { // str == "backpack") {
    width = 0.5;
    height = 0.6;
    offset_z = height/2;
  } else if (classin == 7) { //str == "valve") {
    width = 0.6;
    height = 4.6;
    offset_z = height/2;
  } else if (classin == 6) { //str == "toolbox") {
    width = 0.8;
    height = 0.4;
    offset_z = height/2;
  } else if (classin == 3) { //str == "extinguisher") {
    width = 0.3;
    height = 0.7;
    offset_z = height/2;
  } else if (classin == 4) {//str == "phone") {
    width = 0.1;
    height = 0.15;
    offset_z = height/2;
  } else if (classin == 5) {// str == "radio") {
    width = 0.15;
    height = 0.2;
    offset_z = height/2;
  } else if (classin == 2) {// str == "electrical box" || str == "electricalbox") {
    width = 0.6;
    height = 0.4;
    offset_z = height/2;
  } else {
    width = 0.2;
    height = 0.2;
    offset_z = height/4;
    //return false;
  }
  return true;
}

void callback(const sensor_msgs::ImageConstPtr& image, const sensor_msgs::CameraInfoConstPtr& cam_info, 
              const vision_msgs::Detection2DArrayConstPtr& detections)
{
  static int ind = 0;
  static ros::Time last_save = ros::Time(0);
  if (detections->detections.size() == 0 || detections->detections[0].results.size() == 0)
    return; // No detections here, an empty detection result

  // Publish out the image again
  object_image_pub.publish(image);
  // Save the image
  //
  bool save_result = false;
  cv_bridge::CvImageConstPtr imptr = cv_bridge::toCvShare(image);
  cv::Mat outimg;
  cv::cvtColor(imptr->image, outimg, CV_BGR2RGB);
  std::ofstream ofile;
  if (ros::Time::now() - last_save > ros::Duration(image_save_blanking_interval)) {
    save_result = true;
    if (!boost::filesystem::exists(dirname_to_save))
      boost::filesystem::create_directory(dirname_to_save);
    if (!boost::filesystem::is_directory(dirname_to_save)) {
      ROS_ERROR_STREAM("Warning!: "<< dirname_to_save << " is not a regular directory!  Not saving results");
      save_result = false;
    } else {
      std::stringstream ss;
      do {
        ss.str(std::string(""));
        ss << dirname_to_save << "/" << std::setw(6) << std::setfill('0') << ind++ << ".jpg"; 
      } while(boost::filesystem::exists(ss.str()));
      cv::imwrite(ss.str().c_str(), outimg);    
      ss.str(std::string(""));
      ss << dirname_to_save << "/" << std::setw(6) << std::setfill('0') << ind-1 << ".txt";
      ofile.open(ss.str().c_str(), std::ios::out);
      last_save = ros::Time::now();
    }
  }
  //Go through each detection and add it to the list of detections
  image_geometry::PinholeCameraModel model_;
  model_.fromCameraInfo(*cam_info);
  for (size_t i = 0; i < detections->detections.size(); i++) {
    for (size_t j = 0; j < detections->detections[i].results.size(); j++) {
      //Get approximate bounding box of this projection
      double width, height;
      double offset_x, offset_y, offset_z;
      int id = detections->detections[i].results[j].id;
      if (!GetMetadata(id, width, height, offset_x, offset_y, offset_z)) {
        ROS_ERROR_STREAM("Didn't get metadata! for class " << id);
        continue;
      }
      geometry_msgs::Pose pose = detections->detections[i].results[j].pose.pose;
      tf2::Quaternion obj_pose_quat;
      tf2::convert(pose.orientation, obj_pose_quat);
      tf2::Vector3 offset(offset_x, offset_y, offset_z);
      tf2::Vector3 offset_local = tf2::Matrix3x3(obj_pose_quat) * offset;
      ROS_INFO_STREAM("detection at " << pose.position.x << ", " << pose.position.y << ", " << pose.position.z);
      geometry_msgs::Pose orig_pose = pose;
      pose.position.x += offset_local.x();
      pose.position.y += offset_local.y();
      pose.position.z += offset_local.z();
      ROS_INFO_STREAM("Offset location changed to " <<pose.position.x << ", " << pose.position.y << ", " << pose.position.z);
      // Convert detection coordinates to image coordinates
      geometry_msgs::TransformStamped transformStamped;
      try {
        transformStamped = tfBuffer->lookupTransform(cam_info->header.frame_id, 
                                                     detections->header.frame_id,
                                                     detections->header.stamp,
                                                     ros::Duration(0.1)); 
      } catch(tf2::TransformException& ex) {
        ROS_ERROR_STREAM("Unable to transform detection to camera frame " << ex.what());
        continue;
      }
      tf2::Transform transf;
      transformStamped.transform.translation.x = 0;
      transformStamped.transform.translation.y = 0;
      transformStamped.transform.translation.z = 0;

      tf2::convert(transformStamped.transform, transf);
      tf2::Vector3 inpt;
      tf2::convert(pose.position, inpt);
      tf2::Vector3 inpt_orig;
      tf2::convert(orig_pose.position, inpt_orig);
      tf2::Vector3 inpt_camera = transf * inpt;
      tf2::Vector3 orig_pose_camera = transf * inpt_orig;
      ROS_INFO_STREAM("Point in camera frame is " << inpt_camera.x() << " " << inpt_camera.y() << " " << inpt_camera.z());
      cv::Point3d xyz(inpt_camera.x(), inpt_camera.y(), inpt_camera.z()); // Logical camera is in local robot coordinates
      cv::Point2d center_projection = model_.project3dToPixel(xyz);
      ROS_INFO_STREAM("Point at xyz " << inpt.x() << ", " << inpt.y() << ", " << inpt.z() 
                      << " projects to " << center_projection.x << ", " 
                      << center_projection.y);
      cv::Point3d xyz_orig (orig_pose_camera.x(), orig_pose_camera.y(), orig_pose_camera.z());
      cv::Point2d center_projection_orig = model_.project3dToPixel(xyz_orig);
      //Get class of this projection
      double imwidth = width * model_.fx() / xyz.z;
      double imheight = height * model_.fy() / xyz.z; 
      if (center_projection.x >= 0 && center_projection.x < outimg.cols &&
          center_projection.y >= 0 && center_projection.y < outimg.rows) {
        double cx = center_projection.x / static_cast<double>(outimg.cols);
        double cy = center_projection.y / static_cast<double>(outimg.rows); 
        double w = imwidth / static_cast<double>(outimg.cols);
        double h = imheight / static_cast<double>(outimg.rows);
        double ulx = cx - w/2.0;
        double uly = cy - h/2.0;
        if (ulx < 0) ulx = 0;
        if (uly < 0) uly = 0;
        if (ulx + w > 1.0) w = 1.0 - ulx;
        if (uly + h > 1.0) h = 1.0 - uly;
        ROS_INFO_STREAM("Object id " << id << " projects to bounding box " 
                        << ulx << " " << uly << " " << w << " " << h);
        ROS_INFO_STREAM("Center " << cx << " " << cy);
        cv::rectangle(outimg, cv::Point(ulx*outimg.cols, uly*outimg.rows),
                     cv::Point((ulx+w)*outimg.cols, (uly+h)*outimg.rows), 
                     CV_RGB(0, 255, 0), 5, 8, 0);
        cv::circle(outimg, cv::Point(cx * outimg.cols, cy * outimg.rows), 10, 
                      CV_RGB(255,0,0));
        cv::imshow("window", outimg);
        cv::waitKey(10);
        if(save_result) {
          std::stringstream ofss;
          ofss << id << " " << cx << " " << cy << " " << w << " " << h;
          ROS_INFO_STREAM("About to write " << ofss.str());
          ofile << ofss.str() << std::endl;
        }
      } else {
        ROS_WARN_STREAM("Object detected but does not project into image. A " 
                        << id << " projects to " << center_projection.x << ", "
                        << center_projection.y);
        continue;
      }
      //Compute the ratio of position for yolo expected format
      //Append the file 
    }
  }
  cv_bridge::CvImage imgmsgout;
  imgmsgout.image = outimg;
  object_image_pub.publish(imgmsgout.toImageMsg());
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "camera_object_projector");
  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");
  tfBuffer = boost::shared_ptr<tf2_ros::Buffer>(new tf2_ros::Buffer);
  tfListener = boost::shared_ptr<tf2_ros::TransformListener>(new tf2_ros::TransformListener(*tfBuffer));
  private_nh.param("dirname_to_save", dirname_to_save, std::string("/var/tmp/images_with_objects"));
  private_nh.param("image_save_blanking_interval", image_save_blanking_interval, 1.0);
  object_image_pub = nh.advertise<sensor_msgs::Image>("image_with_object", 10, true);
  annotated_image_pub = nh.advertise<sensor_msgs::Image>("annotated_image_with_object", 10, true);
  message_filters::Subscriber<sensor_msgs::Image> image_sub(nh, "image", 10);
  message_filters::Subscriber<sensor_msgs::CameraInfo> info_sub(nh, "camera_info", 10);
  message_filters::Subscriber<vision_msgs::Detection2DArray> detect_sub(nh, "object_detections",10);
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::CameraInfo, vision_msgs::Detection2DArray> MySyncPolicy;

  message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(100), image_sub, info_sub, detect_sub);
  sync.registerCallback(boost::bind(&callback, _1, _2, _3));

  ros::spin();
  return 0;
}
