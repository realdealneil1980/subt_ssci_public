#!/usr/bin/env bash
# copy files to docker image and rebuild -- this can take a few minutes
#cd ~/subt_hello_world/docker;
#./build_devel_entry.bash helloworld_devel_entry

# define worlds
worlds=( "tunnel_circuit_practice_0" "simple_tunnel_0" )

# Necessary so gazebo can create a context for OpenGL rendering (even headless)
XAUTH=/tmp/.docker.xauth
if [ ! -f $XAUTH ]
then
    xauth_list=$(xauth nlist $DISPLAY)
    xauth_list=$(sed -e 's/^..../ffff/' <<< "$xauth_list")
    if [ ! -z "$xauth_list" ]
    then
        echo "$xauth_list" | xauth -f $XAUTH nmerge -
    else
        touch $XAUTH
    fi  
    chmod a+r $XAUTH
fi

for world_prefix in "${worlds[@]}"
do
  for i in {1..3}
  do
    
    docker run -dit \
      -e DISPLAY \
      -e QT_X11_NO_MITSHM=1 \
      -e XAUTHORITY=$XAUTH \
      -v "$XAUTH:$XAUTH" \
      -v "/tmp/.X11-unix:/tmp/.X11-unix" \
      -v "/etc/localtime:/etc/localtime:ro" \
      -v "/dev/input:/dev/input" \
      --network host \
      --rm \
      --privileged \
      --security-opt seccomp=unconfined \
      --gpus all \
      osrf/subt-virtual-testbed:latest \
      tunnel_circuit_practice.ign worldName:=$world_prefix$i robotName1:=alpha robotConfig1:=X1_SENSOR_CONFIG_4
 
    sleep 20

    docker run -dit \
      -e DISPLAY \
      -e QT_X11_NO_MITSHM=1 \
      -e XAUTHORITY=$XAUTH \
      -v "$XAUTH:$XAUTH" \
      -v "/tmp/.X11-unix:/tmp/.X11-unix" \
      -v "/etc/localtime:/etc/localtime:ro" \
      -v "/dev/input:/dev/input" \
      --network host \
      --rm \
      --privileged \
      --security-opt seccomp=unconfined \
      --gpus all \
      helloworld_devel_entry:latest \
      helloworld_subt_launch robot.launch name:=alpha other_robot:=bravo

    rosrun rviz rviz -d ~/subt_hello_world/catkin_build_ws/src/helloworld_subt_launch/rviz/subt_comms.rviz &

    sleep 100

    docker ps -q | xargs docker kill    
    pkill -9 rviz
    sleep 60

  done
done
